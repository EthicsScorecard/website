#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import os
import shutil
import json
from mako.template import Template
from mako.lookup import TemplateLookup


###########################
### Basic Housecleaning ###
###########################


# Delete everything in public folder
for root, dirs, files in os.walk("./public/"):
    for f in files:
        os.unlink(os.path.join(root, f))
    for d in dirs:
        shutil.rmtree(os.path.join(root, d))


# Copy over third party js & css files
shutil.copytree("./source/assets/", "./public/assets/")
shutil.move("./public/assets/images/", "./public/images/")

# Create directories & copy files
shutil.copytree("./source/flyers/", "./public/flyers/")
os.mkdir("./public/companies")
os.mkdir("./public/metrics")
os.mkdir("./public/contribute")


#####################
### Generate Data ###
#####################


# Load editor-friendly data files
with open('./source/data/metrics_x_companies.json') as f:
    entries = json.load(f)
with open('./source/data/companies.json') as f:
    companies = json.load(f)
with open('./source/data/metrics.json') as f:
    metrics = json.load(f)


# Helper method to get score type given score value and metric
def get_score_type(score_value, metric_name):
    metric_dict = metrics[metric_name]
    if score_value in metric_dict["GreenValues"]:
        return "Green"
    if score_value in metric_dict["YellowValues"]:
        return "Yellow"
    if score_value in metric_dict["RedValues"]:
        return "Red"
    raise Exception("Error translating score to score type")


# Helper method to get company's full name given shortname
def get_full_company_name(company_shortname):
    return companies[company_shortname]["FullCompanyName"]


# Generate datatables-ready json file from easy-to-edit json
columns = [{"data": "CompanyName", "title": "Company", "name": "Company"}]
for metric in metrics: 
    columns.append({"data": metric, "name": metrics[metric]["FullMetricName"], 
        "title": metrics[metric]["FullMetricName"], "defaultContent": ""})

data = {}
for metric_group in entries:
    for company in entries[metric_group]:
        company_name = company["CompanyName"]
        if company_name not in data:
            data[company_name] = {"CompanyName": {
                "display": get_full_company_name(company_name),
                "shortname": company_name }}
        data[company_name][metric_group] = {
            "display": company["Score"],
            "score_type": get_score_type(company["Score"], metric_group)
        }

formatted_data = [data[item] for item in data] # Reformat into array

os.mkdir("./public/data")
with open('./public/data/scorecard.json', 'w+') as outfile:
    json.dump({'data': formatted_data, 'columns': columns}, outfile)


# Create company-specific metric dict for use in company pages
by_company_data = {}
for metric_group in entries:
    for index, company in enumerate(entries[metric_group]):
        company_name = company["CompanyName"]
        if company_name not in by_company_data:
            by_company_data[company_name] = {}
        metric_shortname = metric_group
        metric_data = entries[metric_shortname][index]
        metric_data.update({
            "FullMetricName": metrics[metric_shortname]["FullMetricName"],
            "ScoreType": get_score_type(metric_data["Score"], metric_shortname)
        })
        by_company_data[company_name][metric_shortname] = metric_data


# Annotate companies dict with count of metrics applied, for company list page
for company_name, company_data in companies.items():
    count = len(by_company_data[company_name]) if company_name in by_company_data else 0
    companies[company_name].update({"MetricCount": count})


# Create metric-specific company dict for us in company pages
by_metric_data = {}
for metric_name, metric_data in entries.items():
    by_metric_data[metric_name] = {}
    for index, company in enumerate(metric_data):
        company_name = entries[metric_name][index]["CompanyName"]
        by_metric_data[metric_name][company_name] = entries[metric_name][index]
        score_type = get_score_type(entries[metric_name][index]["Score"], metric_name)
        by_metric_data[metric_name][company_name].update({"ScoreType": score_type})


# Annotate metrics dict with count of companies evaluated, for metric list page
for metric_name, metric_data in metrics.items():
    count = len(by_metric_data[metric_name]) if metric_name in by_metric_data else 0
    metrics[metric_name].update({"CompanyCount": count})


######################
### Generate Pages ###
######################


# Not sure why we need to put the base_template into mylookup manually, but ¯\_(ツ)_/¯
mylookup = TemplateLookup(directories=["/source/templates/"])
base_template = Template(filename="source/templates/base.html") 
mylookup.put_template(uri="source/templates/base.html", template=base_template)


# Create index page
index_template = Template(filename="source/templates/index.html", lookup=mylookup)
with open('./public/index.html', 'w') as file:
    attributes = { "relative_path": "./" }
    file.write(index_template.render(attributes=attributes))


# Create individual company pages
company_template = Template(filename="source/templates/company.html", lookup=mylookup)
for company_name, company_data in companies.items():
    os.mkdir("./public/companies/" + company_name)
    path = './public/companies/' + company_name + "/index.html"
    with open(path, 'w') as file:
        company_metrics = by_company_data[company_name] if company_name in by_company_data else {}
        attributes = {
            "metrics": company_metrics,
            "relative_path": "../../"
            }
        attributes.update(company_data)
        file.write(company_template.render(attributes=attributes))


# Create individual metrics pages
metric_template = Template(filename="source/templates/metric.html", lookup=mylookup)
for metric_name, metric_data in metrics.items():
    os.mkdir("./public/metrics/" + metric_name)
    path = './public/metrics/' + metric_name + "/index.html"
    with open(path, 'w') as file:
        metric_companies = by_metric_data[metric_name] if metric_name in by_metric_data else {}
        attributes = {
            "companies": metric_companies,
            "relative_path": "../../",
            }
        attributes.update(metric_data)
        file.write(metric_template.render(attributes=attributes))


# Create company list page
company_list_template = Template(filename="source/templates/company_list.html", lookup=mylookup)
with open("./public/companies/index.html", "w") as file:
    attributes = {
        "companies": companies,
        "relative_path": "../"
        }
    file.write(company_list_template.render(attributes=attributes))


# Create metric list page
metric_list_template = Template(filename="source/templates/metric_list.html", lookup=mylookup)
with open("./public/metrics/index.html", "w") as file:
    attributes = {
        "metrics": metrics,
        "relative_path": "../"
        }
    file.write(metric_list_template.render(attributes=attributes))


# Create contributing page
contribute_template = Template(filename="source/templates/contribute.html", lookup=mylookup)
with open("./public/contribute/index.html", "w") as file:
    attributes = { "relative_path": "../" }
    file.write(contribute_template.render(attributes=attributes))