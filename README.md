# Tech Ethics Scorecard

The Tech Ethics Scorecard is a collaborative project whose goal is to evaluate tech companies on the ethical impact of their business/products. 

# Contributing

Before contributing, please read our [Code of Conduct](https://docs.google.com/document/d/11VL1pU7bX5ZKaHwxasIhgs0XBNkONPqhQoFjeRVQ_zE/edit#). If you're
interested in becoming a committed member of the project, check out our
[Governance Document](https://docs.google.com/document/d/157L_gAVVfdRDKP-oirHX7y7X3njAHx8givD2PcmbXGI/edit).

There are many ways you can contribute to the tech ethics scorecard.  Here are the most common methods:

### Add new information

The scorecard displays evaluations of specific companies along specific metrics.  In addition to information about those evaluations/ratings, we also need reliable information about the companies being evaluated as well as the metrics being used to evaluate them.  You can add new information or check the reliability of existing data - both are greatly appreciated.

#### Rate companies on existing metrics

To rate a company on an existing metric, first check that you understand the metric and evaluation criteria.  Then, create a new issue in the issue tracker titled in the following format: [Rating: $company_name x $metric].  In the issue, please propose a score along with an explanation for the score.  If possible, also supply articles about that company's performance on the issue, groups working on that issue (especially at that specific company), and any flyers that have been created on that issue (especially that issue at that specific company).

If an issue like this already exists, please feel free to add additional information or correct information that you think is wrong.  If the thread has been closed, you can re-open it to add or correct information.

#### Help define a new metric

The scorecard displays two kinds of metrics - external metrics by third parties, and our own metrics.  Please search for threads about existing metrics before adding your own.

To add a new third party metric, add an issue in the issue tracker titled in the following format: [Metric (Third Party): $metric_subject_or_name].  In the issue, please include the third party, a summary of how the metric works, and why you think it should be included.  Please provide or suggest how to assign ratings to "green", "red" and (optionally) "yellow" values.

To add a new metric of our own, add an issue in the issue tracker titled [Metric: $metric_subject_or_name].  In the issue, please include how you think the metric should work and why you think it should be included.  Defining a new metric can be a somewhat lengthy process full of debate, so don't worry too much about getting things right the first time.

#### Update company information

We have relatively minimalist information on companies, but it should be correct.  If companies have missing or incorrect information, please feel free to add it.

### Format information and add to site

The site uses a somewhat ugly json format that may be tricky for some people to add to or edit.  If you're comfortable editing json and using git more generally, you can help translate peoples' additions into pull requests.  We'll be documenting the process soon, but for now ping the site maintainers to learn more.  

### Review pull requests

Once you've been trained on how the site works, you can review pull requests by manually checking contributions for errors as well as by running the proposed changes locally.  We'll be documenting the process soon, but for now ping the site maintainers at ethics.scorecard@gmail.com to learn more.  

### Add new features

There are a small number of new features we want to add to the site.  To give feedback on proposed features, look for issues in the issue tracker labeled 'feature request'.  To implement features or fix bugs, ping the site maintainers at ethics.scorecard@gmail.com to learn more.  

# Governance/Dispute Resolution

This project of ethical evaluation will require a great many judgment calls.  We seek to be fair in our decisions, efficient in our processes, and accountable to our community - but we know we can't always be all three at once.

Our default mode is a lightweight "do-ocracy", with no consensus required to add data or change features of the website.  However any changes can be reverted or paused, if a community member raises a point of disagreement.

If a disagreement is introduced, we will resolve using the following process:

At any point, an admin can categorize a disagreement as “no consensus”, “some consensus” and “full consensus”.  These categories are fuzzy, of course, but generally speaking, we use the following definitions:

* “no consensus”: two or more options with roughly equal support in the community
* “some consensus”: one option has majority support but there is significant opposition
* “full consensus”: most or all of the community has rallied around a single option

For disagreements about including or defining metrics, we will wait until there’s full consensus before proceeding.  If discussion gets stuck, a majority vote of admins can resolve the issue.

For disagreements about company ratings on a given metric, ratings can be included after they reach “some consensus”.  These ratings will be given an asterisk and a link to the ongoing debate included on the scorecard.  Similarly, if the rating is stuck on “no consensus” the rating will be listed as “undecided” with a link to the debate.

Disagreements about the design and development of the site itself will be left to the discretion of the site founder by default but can be put to a majority vote of admins if any admin requests.

All dispute resolution decisions by admins will be public and admins should always provide their reasoning.





